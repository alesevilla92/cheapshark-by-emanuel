package com.eeema.cheapsharkkotlin.app.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.eeema.cheapsharkkotlin.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
