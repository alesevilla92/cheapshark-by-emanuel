package com.eeema.cheapsharkkotlin.app

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.eeema.cheapsharkkotlin.domain.model.Deal

/**
 * Created by emanuel on 14/01/16.
 */
class DealsAdapter constructor(deals : List<Deal>) : RecyclerView.Adapter<RecyclerView.ViewHolder> (){


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        throw UnsupportedOperationException()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder? {
        throw UnsupportedOperationException()
    }

    override fun getItemCount(): Int {
        throw UnsupportedOperationException()
    }

    class Holder : RecyclerView.ViewHolder{

        constructor(itemView : View) : super(itemView){

        }
    }
}