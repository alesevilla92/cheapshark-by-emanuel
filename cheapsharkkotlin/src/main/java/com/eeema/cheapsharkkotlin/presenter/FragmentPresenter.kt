package com.eeema.cheapsharkkotlin.presenter

/**
 * Created by emanuel on 14/01/16.
 */
public interface FragmentPresenter {

    fun getData();
}