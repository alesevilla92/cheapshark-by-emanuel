package com.eeema.cheapsharkclean.datasources;

import com.eeema.cheapsharkclean.app.preferences.MapperPreferences;

import rx.Observer;

/**
 * Created by Emanuel on 26-10-2015.
 */
public interface Network {

    void getDeals(MapperPreferences mapper, Observer subscriber);

    void getDeal(String dealID,Observer subscriber);

    void getStores(Observer subscriber);

}
