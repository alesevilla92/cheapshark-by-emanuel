package com.eeema.cheapsharkclean.datasources;

import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by V361964 on 26-10-2015.
 */
public class Service {

    public static final String BASE_URL = "http://www.cheapshark.com/api/1.0/";

    public static RestApi getService(){

        OkHttpClient client = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        return  retrofit.create(RestApi.class);
    }

}
