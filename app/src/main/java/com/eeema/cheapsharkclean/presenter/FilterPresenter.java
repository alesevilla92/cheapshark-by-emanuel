package com.eeema.cheapsharkclean.presenter;

import java.util.HashMap;

/**
 * Created by emanuel on 14/12/15.
 */
public interface FilterPresenter {

    void saveInPreferences(HashMap<String,Object> values);
}
