package com.eeema.cheapsharkclean.presenter;

import android.support.v4.app.Fragment;

/**
 * Created by emanuel on 7/11/15.
 */
public interface MainPresenter {

    Fragment selectFragment(int position);
    String getPageTitle(int position);
    void showError(String message);
}
