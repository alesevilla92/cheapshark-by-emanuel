package com.eeema.cheapsharkclean.repository;

import rx.Observer;

/**
 * Created by V361964 on 26-10-2015.
 */
public interface Repository {

    void getDeals(Observer subscriber);

    void getDeal(String dealID);

    void getStores(Observer subscriber);

}
