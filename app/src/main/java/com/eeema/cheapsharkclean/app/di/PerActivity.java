package com.eeema.cheapsharkclean.app.di;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by emanuel on 7/12/15.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {}
