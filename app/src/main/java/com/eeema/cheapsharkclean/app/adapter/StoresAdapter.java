package com.eeema.cheapsharkclean.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.domain.model.Store;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by V361964 on 26-10-2015.
 */
public class StoresAdapter extends RecyclerView.Adapter {

    private static final String TAG = StoresAdapter.class.getName();

    private List<Store> stores;
    private LayoutInflater inflater;
    private Observer subscriber;

    public StoresAdapter(List<Store> stores){
        this.stores = stores;
        this.inflater = LayoutInflater.from(CheapSharkApplication.getsInstance());
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View parentView = inflater.inflate(R.layout.stores_item_layout,parent,false);
        return new Holder(parentView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Store store = stores.get(position);

        changeText((Holder)holder,store);
        manageClick((Holder)holder, store);

    }

    private void changeText(final Holder holder, final Store store){
        holder.name.setText(store.getStoreName());
    }

    public void addSubscriber(Observer subscriber){
        this.subscriber = subscriber;
    }

    private void manageClick(final Holder holder, final Store store){
        holder.name.setOnClickListener(v->{
            final String storeId = store.getStoreID();
            Observable.just(Integer.valueOf(storeId))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
       });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return stores.size();
    }

    static class Holder  extends RecyclerView.ViewHolder{
        @Bind(R.id.name)
        public TextView name;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
