package com.eeema.cheapsharkclean.app.view;

import android.support.v7.widget.RecyclerView;

/**
 * Created by V361964 on 26-10-2015.
 */
public interface FragmentView  {


    void showData(RecyclerView.Adapter   adapter);
    void showErrorMessage(String message);
    void showEmptyView();
    void hideEmptyView();
    void showDealsView();
    void hideDealsView();
    void refreshData();
    void finishRefreshing();
}
