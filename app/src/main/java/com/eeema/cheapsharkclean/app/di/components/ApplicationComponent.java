package com.eeema.cheapsharkclean.app.di.components;

import android.content.Context;

import com.eeema.cheapsharkclean.app.di.modules.ApplicationModule;
import com.eeema.cheapsharkclean.app.preferences.CheapSharkPreferences;
import com.eeema.cheapsharkclean.datasources.Network;
import com.eeema.cheapsharkclean.repository.Repository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@Singleton
@Component(modules =
        {
                ApplicationModule.class
        })
public interface ApplicationComponent {

    void inject(Context context);

    Context context();
    Repository cheapSharkRepository();
    Network cheapSharkNetwork();
    CheapSharkPreferences preferences();
}
