package com.eeema.cheapsharkclean.app.subscribers;

import android.util.Log;

import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.preferences.CheapSharkPreferences;
import com.eeema.cheapsharkclean.app.preferences.PreferenceConstants;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.exceptions.InvalidValueType;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

import rx.Observer;

/**
 * Created by emanuel on 16/12/15.
 */
public class StoresClickSubscriber implements Observer<Integer> {

    private static final String TAG = StoresClickSubscriber.class.getName();

    private MainView mainView;

    public StoresClickSubscriber(MainView v){
        this.mainView = v;
    }


    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        //TODO manage error
    }

    @Override
    public void onNext(Integer storeId) {

        try {
            storeInPreferences(storeId);
            manageView();

        } catch (InvalidValueType invalidValueType) {
            Log.i(TAG,"Cannot be stored in preferences");

        }
    }

    private void manageView(){
        mainView.changePageIndex(FragmentTypes.DEALS_FRAGMENT);
        mainView.changePage(FragmentTypes.DEALS_FRAGMENT);
        mainView.updateView();
    }

    private void storeInPreferences(Integer value) throws InvalidValueType {
        CheapSharkPreferences prefs = CheapSharkApplication.getsInstance().getPreferences();
        prefs.put(PreferenceConstants.STOREID, value);
        prefs.commit();
    }
}
