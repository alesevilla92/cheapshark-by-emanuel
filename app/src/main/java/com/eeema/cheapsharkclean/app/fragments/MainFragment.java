package com.eeema.cheapsharkclean.app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.internal.widget.ViewStubCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.activities.MainActivity;
import com.eeema.cheapsharkclean.app.adapter.StoresAdapter;
import com.eeema.cheapsharkclean.app.di.components.DaggerFragmentComponent;
import com.eeema.cheapsharkclean.app.di.modules.FragmentModule;
import com.eeema.cheapsharkclean.app.di.modules.MainModule;
import com.eeema.cheapsharkclean.app.subscribers.StoresClickSubscriber;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.presenter.FragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Emanuel on 26-10-2015.
 */
public class MainFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{


    @Bind(R.id.deals_list)
    RecyclerView itemList;

    @Bind(R.id.empty_view)
    ViewStubCompat emptyView;

    @Bind(R.id.coordinator_parent)
    CoordinatorLayout coordinatorLayout;

    @Bind(R.id.swipe_layout)
    SwipeRefreshLayout pullToRefreshLayout;

    @Inject
    FragmentPresenter presenter;

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
            boolean enabled =manager.findFirstCompletelyVisibleItemPosition() == 0;
            pullToRefreshLayout.setEnabled(enabled);
        }
    };

    public static MainFragment newInstance(int index){
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(INDEX_KEY, index);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.main_fragment,container,false);
        ButterKnife.bind(this,rootView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        itemList.setHasFixedSize(true);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        itemList.setLayoutManager(manager);
        itemList.addOnScrollListener(scrollListener);

        pullToRefreshLayout.setColorSchemeColors(Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW);
        pullToRefreshLayout.setOnRefreshListener(this);

        emptyView.setVisibility(View.VISIBLE);

        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDagger();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    private void initializeDagger(){
        DaggerFragmentComponent.builder()
                .applicationComponent(CheapSharkApplication.getsInstance().getApplicationComponent())
                .mainModule(new MainModule((MainActivity)getActivity()))
                .fragmentModule(new FragmentModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void showData(RecyclerView.Adapter  adapter) {
        if(adapter instanceof StoresAdapter) {
            ((StoresAdapter)adapter).addSubscriber(new StoresClickSubscriber((MainView)getActivity()));
        }
        itemList.setAdapter(adapter);
    }

    @Override
    public void showErrorMessage(String message) {
        ((MainView)getActivity()).showSnackBar(message);
    }

    @Override
    public void showEmptyView() {
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void showDealsView() {
        itemList.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDealsView() {
        itemList.setVisibility(View.GONE);
    }

    @Override
    public void refreshData() {
        presenter.getData();
    }

    @Override
    public void finishRefreshing() {
        if(pullToRefreshLayout.isRefreshing()){
            pullToRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    public void scrollUp() {
        if(itemList != null){
            itemList.smoothScrollToPosition(0);

        }
    }

}
