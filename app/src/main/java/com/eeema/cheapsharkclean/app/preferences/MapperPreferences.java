package com.eeema.cheapsharkclean.app.preferences;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;

/**
 * Created by emanuel on 14/12/15.
 */
public class MapperPreferences {

    private CheapSharkPreferences preferences;

    public MapperPreferences(){
       preferences = CheapSharkApplication.getsInstance().getPreferences();
    }

    public String getSortBy(){
        return CheapSharkApplication.getsInstance().getResources().getStringArray(R.array.spinner_sortby_entries)[preferences.getSortBy()];
    }
    public int getLowerPrice(){
        return preferences.getLowerPrice();
    }
    public int getUpperPrice(){
        return preferences.getUpperPrice();
    }
    public int getMetacritic(){
        return preferences.getMinimumMetacritic();
    }

    public int getDesc(){
        return preferences.getDescSort() ? 1:0;
    }

    public int getExactTitle(){
        return preferences.getExactTitle() ? 1:0;
    }
    public int getSteamworks(){
        return preferences.getSteamOnly() ? 1 :0;
    }
    public int getOnSale(){
        return  preferences.getOnSale() ? 1 : 0;
    }
    public int getRetailPrice(){
        return  preferences.getRetailPrice() ? 1:0;
    }

    public String getTitle(){return preferences.getTitle();}

    public int getStoreId(){return preferences.getStoreId();}

    public int getPageSize(){return preferences.getPageSize();}

}