package com.eeema.cheapsharkclean.app.di.components;

import com.eeema.cheapsharkclean.app.activities.FilterActivity;
import com.eeema.cheapsharkclean.app.di.PerActivity;
import com.eeema.cheapsharkclean.app.di.modules.FilterModule;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = { FilterModule.class})
public interface FilterComponent {
    void inject(FilterActivity filterActivity);
}
