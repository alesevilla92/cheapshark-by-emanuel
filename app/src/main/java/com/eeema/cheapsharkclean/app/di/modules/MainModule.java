package com.eeema.cheapsharkclean.app.di.modules;

import android.content.Intent;

import com.eeema.cheapsharkclean.app.activities.FilterActivity;
import com.eeema.cheapsharkclean.app.activities.MainActivity;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.presenter.MainPresenter;
import com.eeema.cheapsharkclean.presenter.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 7/12/15.
 */
@Module
public class MainModule {

    private MainActivity mainActivity;

    public MainModule(MainActivity activity){
        this.mainActivity = activity;
    }

    @Provides
    public MainPresenter providesMainPresenter(){
        return new MainPresenterImpl(mainActivity);
    }

    @Provides
    public MainView provideMainView(){
        return mainActivity;
    }

    @Provides
    public Intent provideIntentFilter(){
        return new Intent(mainActivity, FilterActivity.class);
    }

}

