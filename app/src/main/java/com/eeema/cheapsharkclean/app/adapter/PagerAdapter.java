package com.eeema.cheapsharkclean.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.eeema.cheapsharkclean.presenter.MainPresenter;

import javax.inject.Inject;

/**
 * Created by emanuel on 7/12/15.
 */
public class PagerAdapter extends FragmentPagerAdapter {

    private final int NUM_OF_TABS = 2;

    MainPresenter presenter;

    @Inject
    public PagerAdapter(FragmentManager fm, MainPresenter presenter) {
        super(fm);
        this.presenter = presenter;
    }


    @Override
    public Fragment getItem(int position) {
        return presenter.selectFragment(position);
    }

    @Override
    public int getCount() {
        return NUM_OF_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return presenter.getPageTitle(position);
    }

}
