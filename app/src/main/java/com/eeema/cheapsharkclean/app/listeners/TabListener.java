package com.eeema.cheapsharkclean.app.listeners;

import android.view.View;
import android.view.ViewGroup;

import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.app.view.SlidingTabLayout;

import javax.inject.Inject;

/**
 * Created by emanuel on 15/12/15.
 */
public class TabListener implements View.OnClickListener {

    private SlidingTabLayout parent;
    private MainView view;

    @Inject
    public TabListener(SlidingTabLayout parent, MainView view){
        this.parent = parent;
        this.view = view;
    }

    @Override
    public void onClick(View v) {
        ViewGroup container = (ViewGroup) parent.getChildAt(0);
        for (int i = 0; i < container.getChildCount(); i++) {
            if(v == container.getChildAt(i)){
                view.manageClick(i);
            }
        }
    }
}
