package com.eeema.cheapsharkclean.app.preferences;

/**
 * Created by emanuel on 14/12/15.
 */
public class PreferenceConstants {

    public static final String PREFERENCES_NAME = "cheapshark_prefs";

    public static final String SORT_ORDER = "sort_order";
    public static final String LOWER_PRICE = "lower_price";
    public static final String UPPER_PRICE = "upper_price";
    public static final String MINIMUM_METACRITIC = "minimun_metacritic";
    public static final String DESC = "desc_order";
    public static final String EXACT_TITLE = "exact_title";
    public static final String RETAIL_PRICE = "retail_price";
    public static final String STEAMWORKS = "steamworks";
    public static final String ON_SALE = "onsale";
    public static final String PAGE_SIZE = "page_size";
    public static final String STOREID = "store_id";
    public static final String TITLE = "title";


    public static final int DEFAULT_PAGE_SIZE = 60;
    public static final int DEFAULT_UPPER_PRICE= 50;
    public static final int DEFAULT_LOWER_PRICE= 0;
    public static final int DEFAULT_MINIMUM_METACRITIC = 50;
    public static final int DEFAULT_SORT_ORDER = 0;
}
