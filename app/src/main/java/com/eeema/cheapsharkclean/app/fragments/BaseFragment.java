package com.eeema.cheapsharkclean.app.fragments;

import android.support.v4.app.Fragment;

import com.eeema.cheapsharkclean.app.view.FragmentView;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

/**
 * Created by emanuel on 9/12/15.
 */
public abstract  class BaseFragment  extends Fragment implements FragmentView {
    public static final String INDEX_KEY = "index";

    public int getType(){
        if(getArguments() == null) return FragmentTypes.DEALS_FRAGMENT;
        return  getArguments().getInt(INDEX_KEY);
    }

}
