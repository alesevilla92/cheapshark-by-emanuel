package com.eeema.cheapsharkclean.app.view;

/**
 * Created by emanuel on 14/12/15.
 */
public interface FilterView {

    void valueCannotBeStored(String valueKey);
    void commitFinished();
}
