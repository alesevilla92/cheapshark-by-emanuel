package com.eeema.cheapsharkclean.app.di.modules;

import com.eeema.cheapsharkclean.app.activities.FilterActivity;
import com.eeema.cheapsharkclean.app.view.FilterView;
import com.eeema.cheapsharkclean.presenter.FilterPresenter;
import com.eeema.cheapsharkclean.presenter.FilterPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 14/12/15.
 */
@Module
public class FilterModule {

    private FilterActivity filterActivity;

    public FilterModule(FilterActivity activity){
        this.filterActivity = activity;
    }

    @Provides
    public FilterPresenter providesPresenter(){
        return new FilterPresenterImpl(filterActivity);
    }

    @Provides
    public FilterView provideMainView(){
        return filterActivity;
    }
}
