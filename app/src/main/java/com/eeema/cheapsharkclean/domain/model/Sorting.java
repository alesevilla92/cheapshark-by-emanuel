package com.eeema.cheapsharkclean.domain.model;

/**
 * Created by emanuel on 7/11/15.
 */
public enum Sorting {

    DEAL_RATING("Deal Rating"),
    TITLE("Title"),
    SAVINGS("Savings"),
    PRICE("Price"),
    METACRITIC("Metacritic"),
    RELEASE("Release"),
    STORE("Store");

    private String value;

    Sorting(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
