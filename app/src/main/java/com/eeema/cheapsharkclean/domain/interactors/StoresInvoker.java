package com.eeema.cheapsharkclean.domain.interactors;

import com.eeema.cheapsharkclean.repository.Repository;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by V361964 on 26-10-2015.
 */
public class StoresInvoker implements Invoker {

    private Repository repository;

    @Inject
    public StoresInvoker(Repository repository){
        this.repository = repository;
    }

    @Override
    public void execute(Observer subscriber) {
        repository.getStores(subscriber);
    }
}
