package com.eeema.cheapsharkclean.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by emanuel on 25/10/15.
 */
public class Deal {

    @SerializedName("internalName")
    private String internalName;
    @SerializedName("title")
    private String title;
    @SerializedName("metacriticLink")
    private String metacriticLink;
    @SerializedName("dealID")
    private String id;
    @SerializedName("storeID")
    private String storeID;
    @SerializedName("gameID")
    private String gameID;
    @SerializedName("salePrice")
    private String salePrice;
    @SerializedName("normalPrice")
    private String normalPrice;
    @SerializedName("savings")
    private String savings;
    @SerializedName("metacriticScore")
    private String metacriticScore;
    @SerializedName("releaseDate")
    private long releaseDate; //timestamp
    @SerializedName("lastChange")
    private long lastChange; //timestamp
    @SerializedName("dealRating")
    private String dealRating;
    @SerializedName("thumb")
    private String thumb;

    public String getInternalName() {
        return internalName;
    }

    public String getTitle() {
        return title;
    }

    public String getMetacriticLink() {
        return metacriticLink;
    }

    public String getId() {
        return id;
    }

    public String getStoreID() {
        return storeID;
    }

    public String getGameID() {
        return gameID;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public String getNormalPrice() {
        return normalPrice;
    }

    public String getSavings() {
        return savings;
    }

    public String getMetacriticScore() {
        return metacriticScore;
    }

    public long getReleaseDate() {
        return releaseDate;
    }

    public long getLastChange() {
        return lastChange;
    }

    public String getDealRating() {
        return dealRating;
    }

    public String getThumb() {
        return thumb;
    }
}
