package com.eeema.cheapsharkclean.domain.interactors;

import com.eeema.cheapsharkclean.repository.Repository;

import rx.Observer;

/**
 * Created by V361964 on 26-10-2015.
 */
public class DealsInvoker implements Invoker {

    private Repository repository;


    public DealsInvoker( Repository repository){
        this.repository = repository;
    }

    @Override
    public void execute(Observer subscriber) {
        repository.getDeals(subscriber);
    }
}
