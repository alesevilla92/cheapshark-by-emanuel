package com.eeema.cheapsharkclean.datasources;

import android.content.res.AssetManager;
import android.util.Log;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.adapter.DealsAdapter;
import com.eeema.cheapsharkclean.app.adapter.StoresAdapter;
import com.eeema.cheapsharkclean.app.preferences.MapperPreferences;
import com.eeema.cheapsharkclean.domain.model.Deal;
import com.eeema.cheapsharkclean.domain.model.Store;
import com.eeema.cheapsharkclean.exceptions.NoDataRetrieve;
import com.eeema.cheapsharkclean.utils.ConverterUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emanuel on 8/01/16.
 */
public class FakeNetworkImpl implements Network{


    private String jsonStringFromFile(String filename) throws IOException {
        AssetManager assets = CheapSharkApplication.getsInstance().getAssets();
        InputStream stream = assets.open(filename);
        String jsonResponse = ConverterUtils.convertInputStreamToString(stream);
        return jsonResponse;
    }

    private List<Deal> parseDeals(){
        try {
            String json = jsonStringFromFile("deals.json");
            Gson gson = new Gson();
            Type responseType = new TypeToken<List<Deal>>(){}.getType();
            return gson.fromJson(json, responseType);

        } catch (IOException e) {
            Log.e(FakeNetworkImpl.class.getName(),"Error parsing file");
            return new ArrayList<Deal>();
        }

    }
    private List<Store> parseStores(){
        try {
            String json = jsonStringFromFile("stores.json");
            Gson gson = new Gson();
            Type responseType = new TypeToken<List<Store>>(){}.getType();
            return gson.fromJson(json, responseType);

        } catch (IOException e) {
            Log.e(FakeNetworkImpl.class.getName(),"Error parsing file");
            return new ArrayList<Store>();
        }

    }

    @Override
    public void getDeals(MapperPreferences mapper, Observer subscriber) {
        Observable.just(parseDeals()).flatMap(deals -> {
            if(deals != null  &&  !deals.isEmpty()){
                return Observable.just(new DealsAdapter(deals));
            }else{
                return generateErrorObservable(subscriber);
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void getDeal(String dealID, Observer subscriber) {

    }

    @Override
    public void getStores(Observer subscriber) {
        Observable.just(parseStores()).flatMap(stores -> {

            if(stores  == null || stores.isEmpty()){
                return generateErrorObservable(subscriber);
            }else{
                return Observable.just(new StoresAdapter(stores));
            }

        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    private Observable generateErrorObservable(Observer subscriber){
        subscriber.onError(new NoDataRetrieve(CheapSharkApplication.getsInstance().getString(R.string.no_data_retrieve_message)));
        subscriber.onCompleted();
        return Observable.empty();
    }
}
