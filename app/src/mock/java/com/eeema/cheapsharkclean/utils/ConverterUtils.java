package com.eeema.cheapsharkclean.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by emanuel on 8/01/16.
 */
public class ConverterUtils {

    public static String convertInputStreamToString(InputStream stream){

        try {
            BufferedReader reader= new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            return builder.toString();

        } catch (IOException e) {
            Log.e(ConverterUtils.class.getName(), "An exception happened reading from input stream");
            return "";
        }

    }
}
