package com.eeema.cheapsharkclean.datasources;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.adapter.DealsAdapter;
import com.eeema.cheapsharkclean.app.adapter.StoresAdapter;
import com.eeema.cheapsharkclean.app.preferences.MapperPreferences;
import com.eeema.cheapsharkclean.domain.model.Deal;
import com.eeema.cheapsharkclean.exceptions.NoDataRetrieve;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by V361964 on 26-10-2015.
 */
public class NetworkImpl implements Network {

    private RestApi restApi;

    @Inject
    public NetworkImpl(){
        restApi = Service.getService();
    }

    @Override
    public void getDeals(MapperPreferences mapper, Observer subscriber) {

        Observable<List<Deal>> observable = createObservable(mapper);
        observable.subscribeOn(Schedulers.newThread())

                .observeOn(AndroidSchedulers.mainThread())

                .flatMap(deals -> {
                    if(deals != null  &&  !deals.isEmpty()){
                        return Observable.just(new DealsAdapter(deals));
                    }else{
                        return generateErrorObservable(subscriber);
                    }
                })
                .subscribe(subscriber);


    }

    private Observable<List<Deal>> createObservable(MapperPreferences mapperPreferences){
        Observable<List<Deal>> observable = createObservableDefault(mapperPreferences);

        if(mapperPreferences.getStoreId() != Integer.MIN_VALUE){
            observable = createObservableStore(mapperPreferences);
        }

        if(mapperPreferences.getTitle() != null &&  !mapperPreferences.getTitle().isEmpty()){
            observable = createObservableTitle(mapperPreferences);
        }

        return observable;
    }

    private Observable<List<Deal>> createObservableDefault(MapperPreferences mapper){
        return restApi.getDeals(
                mapper.getPageSize(),
                mapper.getSortBy(),
                mapper.getDesc(),
                mapper.getLowerPrice(),
                mapper.getUpperPrice(),
                mapper.getMetacritic(),
                mapper.getRetailPrice(),
                mapper.getSteamworks(),
                mapper.getOnSale());


    }
    private Observable<List<Deal>> createObservableStore(MapperPreferences mapper){
        return restApi.getDeals(
                mapper.getStoreId(),
                mapper.getPageSize(),
                mapper.getSortBy(),
                mapper.getDesc(),
                mapper.getLowerPrice(),
                mapper.getUpperPrice(),
                mapper.getMetacritic(),
                mapper.getRetailPrice(),
                mapper.getSteamworks(),
                mapper.getOnSale());


    }
    private Observable<List<Deal>> createObservableTitle(MapperPreferences mapper){
        return restApi.getDeals(
                mapper.getPageSize(),
                mapper.getSortBy(),
                mapper.getDesc(),
                mapper.getLowerPrice(),
                mapper.getUpperPrice(),
                mapper.getMetacritic(),
                mapper.getTitle(),
                mapper.getExactTitle(),
                mapper.getRetailPrice(),
                mapper.getSteamworks(),
                mapper.getOnSale());


    }
    @Override
    public void getDeal(String dealID, Observer subscriber) {

    }

    @Override
    public void getStores(Observer subscriber) {
        restApi
                .getStores()

                .subscribeOn(Schedulers.newThread())

                .observeOn(AndroidSchedulers.mainThread())

                .flatMap(stores -> {

                    if(stores  == null || stores.isEmpty()){
                        return generateErrorObservable(subscriber);
                    }else{
                        return Observable.just(new StoresAdapter(stores));
                    }

                })
                .subscribe(subscriber);
    }

    private Observable generateErrorObservable(Observer subscriber){
        subscriber.onError(new NoDataRetrieve(CheapSharkApplication.getsInstance().getString(R.string.no_data_retrieve_message)));
        subscriber.onCompleted();
        return Observable.empty();
    }
}
