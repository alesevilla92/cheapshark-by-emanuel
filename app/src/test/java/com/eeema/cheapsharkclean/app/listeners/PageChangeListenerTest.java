package com.eeema.cheapsharkclean.app.listeners;

import com.eeema.cheapsharkclean.app.view.MainView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Created by emanuel on 11/01/16.
 */
public class PageChangeListenerTest {

    PageChangeListener listener;

    @Mock
    MainView view;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        listener = new PageChangeListener(view);
    }

    @Test
    public void onPageChanged(){
        listener.onPageSelected(0);
        verify(view).changePageIndex(0);
    }

}